﻿SELECT  dnombre, COUNT( emp_no)  NUMERO_DE_EMPLEADOS

FROM depart LEFT JOIN emple USING(dept_no)

GROUP BY dept_no;


-- **** c1   *****
SELECT dept_no, COUNT(*) NUMERO_DE_EMPLEADOS  FROM emple GROUP BY dept_no;


-- **** c2 ******
SELECT dnombre, COUNT(*) NUMERO_DE_EMPLEADOS  FROM emple  JOIN depart USING (dept_no) GROUP BY dept_no;


SELECT dnombre, IFNULL(n,0) NUMERO_DE_EMPLEADOS  FROM depart LEFT JOIN (SELECT dept_no, COUNT(*) n  FROM emple GROUP BY dept_no) c1  USING (dept_no) ORDER BY NUMERO_DE_EMPLEADOS DESC ;


SELECT * FROM


