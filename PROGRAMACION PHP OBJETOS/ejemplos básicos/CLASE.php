<?php

class Vehiculo 
	{
	public $velocidad;
	};

$coche = new Vehiculo;

// mostrar todas las propiedades de un objeto

echo '<pre>';
print_r($coche);
echo '</pre>';


// así no se puede meter un valor a un
// objeto aunque parezca un array $coche['velocidad']=5;


// meter valor a un atributo de un objeto
$coche->velocidad=5;

// mostrar el atributo de un objeto
echo '<pre>';
echo $coche->velocidad;
echo '</pre>';



?>

