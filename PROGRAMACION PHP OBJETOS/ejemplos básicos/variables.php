<?php
// variable simple
$a='Yo';


echo '<pre>';
echo $a;
echo '</pre>';

/// variable array 

$b[1]='Hola';
$b[2]='Adios';

echo '<pre>';
echo $b[1];
echo '</pre>';

echo '<pre>';
echo $b[2];
echo '</pre>';

// variable array dentro de una variable array


$b[3]=
	[
	'81'=>'buenos días',
	'50'=>'buenos tardes',
	];
echo '<pre>';
print_r ($b);
echo '</pre>';

echo '<pre>';
print_r ($b[3]);
echo '</pre>';


echo '<pre>';
echo $b[3][81];
echo '</pre>';


// añadir otro array

$b[4]='buenas noches';

echo '<pre>';
echo $b[4];
echo '</pre>';






?>