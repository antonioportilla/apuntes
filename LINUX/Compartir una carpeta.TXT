COMPARTIR UNA CARPETA

poner sudo comando da al comando privilegios de administrador, si no se pone sudo delante se ejecuta el comando con privilegios
de usuario estandar, esto vale de explicacion para todos los comandos.

1º Instalar SAMBA
sudo apt-get install samba
---------------

2º sudo nano /etc/hostname "Se puede cambiar el nombre del host"
--------------------

3º podemos crear unas carpeta para compartirlas que cuelguen de la raiz.
sudo mkdir /compartir 
sudo mkdir /compartir/publico
sudo mkdir /compartir/user
-------------------------------
cd / "accedemos a la carpeta raiz del sistema"
pwd "sale la ruta en la que estamos actualmente debiara salir / "
ls -la "muestra el contenido de la carpeta actual que se escribe con un: . se puede omitir, al poner -la sale lo oculto y muestra usuarios y permisos  "

4º A esas carpetas las damos permisos de escritira lectura y ejecución, para el propietario, grupo y otros usuarios.

sudo chmod -R 777 compartir  "-R ojo la R en mayuscula, con ese opción da los mismos privilegios a las subcarpetas que contiene"

3º Añadimos el usuario user con cualquier password user por ejemplo al sistema samba de compartir archivos

sudo pdbedit -L "con este comando se ven los usuarios de samba en marcha, lo normal es que no exista ninguno"

sudo useradd user "con eso añadimos un usuario llamado user al sistema"

sudo userdel -r usuario "no hay que ejecutarlo, pero es la forma de borrarlo"

sudo smbpasswd -a user "con eso añadimos un usuario a samba"

sudo pdbedit -L "con este comando se ven los usuarios de samba deberia de poner user en el caso que no, volver a ejecutar el 
				 comando anterior y a la hora de poner el password pon algo diferente a user"

sudo smbpasswd -x user "siqueremos borrar el usuario de samba"

sudo groupadd workgroup "añade el grupo workgroup"



4º Configurar la camperta a compartir.
sudo nano /etc/samba/smb.conf "con eso abrimos el editor de textos para añadir el siguinte texto"

# "Se añade estó al final del documento"
# "Se puede cambiar el grupo de trabajo"

[publico]
path = /compartir/publico
read only = no
guest ok = yes
browseable = yes






-----------------------------
--------
3º "se reinicia Samba"
sudo samba restart
"si hay algún error sale aquí"
------------------------
